CONTENTS OF THIS FILE
---------------------

 * About Blocks per menu
 * Requirements
 * Installation
 * Configuration

ABOUT BLOCKS PER MENU
--------------------
Blocks per menu extends the block visibility interface in Drupal 7 and allows
the user to control block visibility based on the menu links.


REQUIREMENTS
------------

This module requires core modules "Menu" and "Block" to be enabled.

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
The module has no menu or modifiable settings. There is no configuration. When
enabled, there is a new vertical settings tab named as "menu" is added in the
block configure page.
